(function(){
    angular
        .module("DMS")
        .config(uiRouteConfig);
    
    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        $stateProvider
            .state("edit", {
                url: "/edit",
                templateUrl: "app/edit/edit.html"
            })
            .state("register", {
                url: "/register",
                templateUrl: "app/registration/register.html"
            })
            .state("search", {
                url: "/search",
                templateUrl: "app/search/search.html"
            })
            .state("searchDB", {
                url: "/searchDB",
                templateUrl: "app/search/searchDB.html"
            })
            .state("editWithParams", { 
               url: "/edit/:dept_no",
               templateUrl: "app/edit/edit.html",
               //controller: "EditCtrl",
               //controllerAs: "ctrl"
           });
    }
})();